# Demo Project to save user profile

View the Live demo in codesandbox [codesandbox] (https://codesandbox.io/s/demo-snq32?file=/src/App.js)

.

### Clone this repo using

`git clone`

### Use npm or yarn to install dependencies

`npm install`
or
`yarn install`

## dependencies

`styled-component`
`react-router-dom`
