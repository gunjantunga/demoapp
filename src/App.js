import React from 'react';
import './App.css';
import LogInPage from './components/log-in-page';
import Profilepage  from './components/profile-page';
import {BrowserRouter as Router,Route,Redirect} from 'react-router-dom';






function App() {

  

  return (
    
    <div className="App">
      <Router>
        <Route exact path='/'>
          <Redirect to='/login'/>
        </Route>
        <Route exact path="/login">
          <LogInPage/>
        </Route>
      <Route path='/profile'>

       <Profilepage/> 
      </Route>
      </Router>
      

    </div>


  );
}

export default App;



