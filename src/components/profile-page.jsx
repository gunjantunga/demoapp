import React, { useEffect, useMemo, useRef, useState } from "react";
import styled from "styled-components";

import SkillList, { ChooseSkillView } from "./skill-list";
import { useHistory } from "react-router-dom";
import LogoutBtn from "./logout";

export default function ProfilePage() {
  const userData = useMemo(() => getUserDataFromStorage(), []);
  const [showDilog, setShowDilog] = useState(false);
  const history = useHistory();

  const nameRef = useRef();
  const ageRef = useRef();
  const genderRef = useRef();

  const [skills, setSkills] = useState(userData.skills);

  const [selectedAddSkill, setSelectedAddSkill] = useState(null);

  function handleAddSkillClick(addSkill) {
    if (addSkill.text === "") {
      setShowDilog(true);
      setSelectedAddSkill(addSkill);
    }
  }

  const handleChooseSkill = (skill) => {
    let _skills = [...skills];

    let findSkill = _skills.find((ele) => ele.id === selectedAddSkill.id);
    //console.log(findSkill);
    findSkill.text = skill;
    console.log(_skills);
    setSkills(_skills);
    setShowDilog(false);
  };

  let handleDelete = (skill) => {
    let _skills = [...skills];
    let findSkill = _skills.find((ele) => ele.id === skill.id);

    findSkill.text = "";

    setSkills(_skills);
  };

  const isAuthenticated = () => {
    let authEmail = window.localStorage.getItem("auth");

    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if (!pattern.test(authEmail)) {
      history.push("/login");
    }
  };

  const handleSubmit = () => {
    let nameTxt = nameRef.current.value;
    let ageTxt = parseInt(ageRef.current.value);
    let genderTxt = genderRef.current.value.toLowerCase();

    if (nameTxt) {
      if (!isNaN(ageTxt)) {
        if (genderTxt === "male" || genderTxt === "female") {
          window.localStorage.setItem(
            "_userData",
            JSON.stringify({
              name: nameTxt,
              age: ageTxt,
              gender: genderTxt,
              skills,
            })
          );
          alert("Your profile data saved successfully");
        } else {
          alert("Gender must be mail/femail");
        }
      } else {
        alert("age must be a number");
      }
    } else {
      alert("Name Cannot be empty");
    }
  };
  useEffect(() => {
    isAuthenticated();
  }, []);

  return (
    <div>
      <Headerbar>
        <LogoutBtn />
      </Headerbar>
      <ProfileText>Welcome to Profile Page</ProfileText>
      <ProfileForm>
        <Holder>
          <label>Enter your name</label>
          <InputText
            type="text"
            placeholder="Enter your name"
            ref={nameRef}
            defaultValue={userData.name}
          />
        </Holder>
        <Holder>
          <label>Enter your age</label>
          <InputText
            type="text"
            placeholder="Enter your age"
            ref={ageRef}
            defaultValue={userData.age}
          />
        </Holder>
        <Holder>
          <label>Enter your gender</label>
          <InputText
            type="text"
            placeholder="Enter your gender"
            ref={genderRef}
            defaultValue={userData.gender}
          />
        </Holder>
        <SkillHeader>Add your skills</SkillHeader>

        <SkillList
          skills={skills}
          handleDelete={handleDelete}
          handleAddSkillClick={handleAddSkillClick}
        />

        {showDilog ? (
          <ChooseSkillView
            skills={skills}
            handleChooseSkill={handleChooseSkill}
          />
        ) : null}
        <SaveProfile onClick={handleSubmit}>Submit</SaveProfile>
      </ProfileForm>
    </div>
  );
}

function getUserDataFromStorage() {
  let userData = window.localStorage.getItem("_userData");
  let defaultSchema = {
    name: "",
    age: "",
    gender: "",
    skills: [
      { id: 1, text: "" },
      { id: 2, text: "" },
      { id: 3, text: "" },
      { id: 4, text: "" },
      { id: 5, text: "" },
    ],
  };
  if (userData) {
    try {
      userData = JSON.parse(userData);
    } catch (err) {
      alert("Error in parsing user data");
      return defaultSchema;
    }
    return userData;
  } else {
    return defaultSchema;
  }
}

const SkillHeader = styled.h2`
  margin-bottom: 20px;
  color: gray;
`;

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  width: 200px;
  margin-bottom: 20px;
`;

const ProfileText = styled.h2`
  text-align: center;
  margin-bottom: 30px;
  margin-top: 20px;
`;

const ProfileForm = styled.div`
  margin-bottom: 30px;

  width: 200px;
  flex-direction: column;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
`;
const InputText = styled.input`
  width: 200px;
  padding: 8px 10px;
  padding-left: 10px;
  margin-top: 10px;
  border-radius: 5px;
  border: 1px solid lightgray;
`;

const Headerbar = styled.div`
  height: 50px;
  background: lightblue;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  padding-right: 20px;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
`;

const SaveProfile = styled.button`
  background-color: lightblue;
  width: 100px;
  padding: 10px 20px;
  border: none;
  align-self: flex-end;
  border-radius: 5px;
  cursor: pointer;
  margin-top: 20px;
`;
