import React from "react";
import styled from "styled-components";

const _SKILLS = ["Javascript", "Python", "PHP", "java", "C++"];

export default function SkillList({
  skills,
  handleDelete,
  handleAddSkillClick,
}) {
  const handleAddSkill = (skill) => {
    handleAddSkillClick(skill);
  };

  const ownHandleDelete = (e, skill) => {
    //alert("cki");
    e.stopPropagation();
    handleDelete(skill);
  };
  return (
    <SkillContainer>
      <ul>
        {skills.map((skill, index) => {
          return (
            <AddSkillHolder key={skill} onClick={() => handleAddSkill(skill)}>
              <AddSkill>
                {skill.text ? (
                  <Selectedskill>
                    {index + 1}. {skill.text}
                  </Selectedskill>
                ) : (
                  <AddSkill>{index + 1}. Add Skill</AddSkill>
                )}
              </AddSkill>
              {skill.text ? (
                <DeleteBtn onClick={(e) => ownHandleDelete(e, skill)}>
                  x
                </DeleteBtn>
              ) : null}
            </AddSkillHolder>
          );
        })}
      </ul>
    </SkillContainer>
  );
}

export function ChooseSkillView({ handleChooseSkill, skills }) {
  return (
    <div>
      <ListView
        skills={filterNotSelected(_SKILLS, skills)}
        handleChooseSkill={handleChooseSkill}
      />
      <Overlay></Overlay>
    </div>
  );
}

function filterNotSelected(arr1, arr2) {
  return arr1.filter((sk) => {
    let notFound = arr2.every((_sk) => {
      return sk !== _sk.text;
    });
    // console.log(noound);
    return notFound;
  });
}

function ListView({ skills, handleChooseSkill }) {
  return (
    <ListContainer>
      <Header>Select a skill to add</Header>
      <ul>
        {skills.map((skill, index) => {
          return (
            <DilogList onClick={() => handleChooseSkill(skill)}>
              <DilogNumber>{index + 1}</DilogNumber>

              <h4>{skill}</h4>
            </DilogList>
          );
        })}
      </ul>
    </ListContainer>
  );
}

const Overlay = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background-color: black;
  opacity: 0.5;
  z-index: 10;
`;

const SkillContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const AddSkillHolder = styled.li`
  width: 200px;
  height: 40px;
  margin-top: 10px;

  display: flex;
  align-items: center;
  padding-left: 20px;
  cursor: pointer;
  list-style: none;
  position: relative;
`;

//const AddSkill = styled.h3``;

const ListContainer = styled.div`
  width: 250px;
  padding: 10px;
  border-radius: 10px;

  position: absolute;
  z-index: 11;
  background: white;
  opacity: 1;
  top: 20%;
  left: 50%;
  margin-left: -150px;
`;

const DilogList = styled.li`
  margin: 10px;
  list-style: none;
  margin-top: 20px;
  display: flex;
  align-items: center;
  cursor: pointer;
  &:hover {
    color: blue;
  }
`;
const Header = styled.h3``;
const DilogNumber = styled.span`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background: gray;
  display: inline-block;
  text-align: center;
  line-height: 30px;
  margin-right: 20px;
`;

const DeleteBtn = styled.span`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background: lightgray;
  display: flex;
  line-height: 30px;
  justify-content: center;
  align-items: center;
  position: absolute;
  right: 10px;
  font-weight: bold;
  font-size: 14px;
`;

const AddSkill = styled.h4`
  color: lightgray;
  display: inline-block;
`;

const Selectedskill = styled.h3`
  color: blue;
  display: inline-block;
`;
