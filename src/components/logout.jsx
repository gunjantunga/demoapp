import React from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

function Logout() {
  let autheEmail = window.localStorage.getItem("auth");
  let history = useHistory();

  const handleLogout = () => {
    window.localStorage.setItem("auth", "");

    history.push("/login");
  };

  return (
    <div>
      {autheEmail && <LogoutBtn onClick={handleLogout}>Logout</LogoutBtn>}
    </div>
  );
}

const LogoutBtn = styled.span`
  font-size: 15px;
  cursor: pointer;
  font-weight: bold;
`;
export default Logout;
