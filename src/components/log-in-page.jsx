import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import { useHistory } from "react-router-dom";

export default function LogInPage() {
  const emailRef = useRef();
  const passwordRef = useRef();
  let history = useHistory();
  const handleLogIn = () => {
    let mailTxt = emailRef.current.value;
    let passwordTxt = passwordRef.current.value;

    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if (pattern.test(mailTxt)) {
      if (passwordTxt.length > 5) {
        if (mailTxt === "admin@admin.com" && passwordTxt === "admin@123") {
          window.localStorage.setItem("auth", mailTxt);

          history.push("/profile");
        } else {
          alert("Invalid user name and password");
        }
      } else {
        alert("Password field is required and minimum five charecters");
      }
    } else {
      alert("Please enter a valid email address");
    }
  };

  const isAuthenticated = () => {
    let authEmail = window.localStorage.getItem("auth");

    var pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

    if (pattern.test(authEmail)) {
      history.push("/profile");
    }
  };

  useEffect(() => {
    isAuthenticated();
  }, []);

  return (
    <div>
      <Headerbar></Headerbar>
      <LogInText>Let's Log In</LogInText>
      <LogInForm>
        <Holder>
          <label>Enter your email address</label>
          <InputText
            type="text"
            ref={emailRef}
            placeholder="Enter your email address"
          />
        </Holder>
        <Holder>
          <label>Enter your password</label>
          <InputText
            type="password"
            ref={passwordRef}
            placeholder="Enter your password"
          />
        </Holder>
        <Holder>
          <LogInButton onClick={handleLogIn}>LogIn</LogInButton>
        </Holder>
      </LogInForm>
    </div>
  );
}

const Holder = styled.div`
  display: flex;
  flex-direction: column;

  width: 200px;
  margin-bottom: 20px;
`;

const LogInText = styled.h2`
  text-align: center;
  margin-bottom: 30px;
  margin-top: 20px;
`;

const LogInForm = styled.div`
  margin-bottom: 30px;
  width: 200px;
  flex-direction: column;
  align-items: center;
  margin-left: auto;
  margin-right: auto;
`;
const InputText = styled.input`
  width: 200px;
  padding: 8px 10px;
  padding-left: 10px;
  margin-top: 10px;
  border-radius: 5px;
  border: 1px solid lightgray;
`;

let LogInButton = styled.button`
  background-color: lightblue;
  width: 100px;
  padding: 10px 20px;
  border: none;
  align-self: flex-end;
  border-radius: 5px;
  cursor: pointer;
`;
const Headerbar = styled.div`
  height: 50px;
  background: lightblue;
  display: flex;
  justifycontent: "flex-end";
`;
